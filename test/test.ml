open Ezjs_min

let%store version = 0
and host = "http://..."

module Parent = struct

  let%data family_name = "duck"
  and kids: string list = [ "riri"; "fifi"; "loulou" ]

end

module Kid = struct

  type%data kid = string

  let%data full_name () : string =
    Format.sprintf "%s %s" (to_string [%data]##.kid) (to_string [%data]##.family_name_)

  let%meth introduce () =
    log "Hello, I am %s" (to_string [%data]##.full_name_)

  let%meth init () =
    log "%s loaded for version %d" (to_string [%data]##.full_name_) [%store]##.version

  [%%alp {parent=Parent; store}]
end

[%%alp {init}]
