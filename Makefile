all: build
build:
	@dune build src
dev:
	@dune build --profile release
	@cp -f _build/default/test/test.bc.js test/test.js
clean:
	@dune clean
