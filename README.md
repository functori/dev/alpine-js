# alpine-js PPX

`alpine-js` is a library and preprocessor to build alpine-js applications in js_of_ocaml

## Providing arguments

- data
```ocaml
let%data message = "Hello, world!"
```
In most cases the data need to be typed but for this example the type is inferred.
This data will be available as the jsoo type. See the [initializing section](#debug) section to show the type that is derived.
If you don't want the data to be converted in jsoo, you can add the `[@noconv]` attribute.

- method
In the same way:
```ocaml
let%meth print (name: string) =
  Format.printf "Hi %s!\n%s" name (Js_of_ocaml.Js.to_string [%data]##.message)
```

- store
To store global values:
```ocaml
let%store config : my_config_type = ...
```

- extra data type
If the component is inheriting other data (for example via `x-for`) that you need in your methods you can add them with:
```ocaml
type%data extra_type = my_core_type
```

## Initializing the data

At the end of every structure (modules or files), a function `init` will be created if you have provided arguments in it. This function will be called with the `alpine:init` event.
You can also add your own init function:
```ocaml
[%%init fun _alpine -> ...]
```
In the same way you can define a function that will be called with `alpine:initialized` event with:
```ocaml
[%%initialized fun _alpine -> ...]
```
If you want to invoke special options, you need to add an item:
```ocaml
[%%alpine { options }]
```
The options are:
- `parent=Module_name`: register a parent to the module, the component will inherit the properties of its parent.
- `store=Module_name` or `store` (if the store is defined in ths file): use the global store in this module.
- `debug`<a name="debug"></a>: print all modified and created items.
- `noconv`: globally disable the automatic conversion of types in jsoo types

In the main file/module of your application, you need to invoke `[%%alpine { options }]` with special options:
- `init=[Module_name1; Module_name2, ...]` or `init` (that will only take into account modules in this file): load the listener for `alpine:init` event.
- `initialized=[Module_name1; Module_name2, ...]` or `initialized`: load the listener for `alpine:initialized` event.

## Accessing the magics

You can access all magic properties of alpine.js:
- `[%el]`: the current node element
- `[%refs element_ref]`: the node element referenced by `element_ref`
- `[%store]`: the global store
- `[%watch target (fun value -> ...)]`: watch a certain target properties
- `[%dispatch event]` or `[%dispatch event msg]`: dispatch an event to parent components
- `[%next expression]`: encapsulate `expression` in `nextTick`
- `[%root]`: the root element
- `[%data]`: the data of this component
- `[%id my_id]`: create a id
- `[%persist value]`: persist a value (using the persist plugin)
