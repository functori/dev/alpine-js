open Ezjs_min

class type element = object
  inherit Dom_html.element
  method dataset: js_string t Table.t readonly_prop
end

type ('data, 'store) instance

class type alpine_ct = object
  method data : js_string t -> (('data, 'store) instance t, 'data) meth_callback -> unit meth
  method store : js_string t -> _ -> unit meth
end

type alpine = alpine_ct t

let lifecycle e (f: alpine -> unit) =
  ignore @@
  Js_of_ocaml.Dom_events.listen Dom_html.document (Dom.Event.make ("alpine:" ^ e)) @@ fun _ _ ->
  let alpine = Unsafe.global##._Alpine in
  f alpine;
  false

let init f = lifecycle "init" f
let initialized f = lifecycle "initialized" f

let fdata (alpine: alpine) name f =
  alpine##data (string name) (wrap_meth_callback f)
let adata alpine name x = fdata alpine name (fun _ -> x)

let astore (alpine: alpine) name arg = alpine##store (string name) arg

let get (i: _ instance t) name = Unsafe.get i (string @@ "$" ^ name)
let call (i: _ instance t) name args = Unsafe.meth_call i ("$" ^ name) args

let el (i: _ instance t) : element t = get i "el"
let refs (i: _ instance t) member : element t optdef = Unsafe.get (get i "refs") (string member)
let store (i : ('data, 'store) instance t) : 'store = get i "store"
let root (i: _ instance t) : element t = get i "root"
let data (i : ('data, 'store) instance t) : 'data = get i "data"

let watch (i: _ instance t) target (f: _ -> unit) : unit =
  call i "watch" [| Unsafe.inject (string target); Unsafe.inject f |]
let dispatch ?msg (i: _ instance t) event : unit =
  let args = match msg with
    | None -> [| Unsafe.inject (string event) |]
    | Some msg -> [| Unsafe.inject (string event); Unsafe.inject msg |] in
  call i "dispatch" args
let next_tick (i: _ instance t) (f: unit -> unit) : unit = call i "nextTick" [| Unsafe.inject f |]
let id (i: _ instance t) s : js_string t = call i "id" [| Unsafe.inject (string s) |]
let persist (i: _ instance t) (x: 'a) : 'a = call i "persist" [| Unsafe.inject x |]
