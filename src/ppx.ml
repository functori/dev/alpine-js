open Ppxlib
open Ast_builder.Default

let longident_pp fmt lid = Format.fprintf fmt "%s" (Longident.name lid)

type element = {
  name: string;
  typ: (core_type [@printer Pprintast.core_type]) option;
  value: expression [@printer Pprintast.expression];
  conv: bool;
  kind: [`meth | `data | `func];
} [@@deriving show]

type acc = {
  modu: string;
  debug: bool;
  convert: bool;
  data: element list;
  store: element list;
  parent: (longident [@printer longident_pp]) option;
  glob: (longident [@printer longident_pp]) option;
  root_init: (longident [@printer longident_pp]) list option;
  root_initialized: (longident [@printer longident_pp]) list option;
  modules_init: (longident [@printer longident_pp]) list;
  modules_initialized: (longident [@printer longident_pp]) list;
  instance: string option;
  init: (expression [@printer Pprintast.expression]) option;
  initialized: (expression [@printer Pprintast.expression]) option;
  extra_data: element list;
} [@@deriving show]

let empty_acc = {
  modu="default"; debug=false; data=[]; store=[]; convert=true;
  parent=None; glob=None; root_init=None; root_initialized=None;
  modules_init=[]; modules_initialized=[];
  instance=None; init=None; initialized=None; extra_data=[] }

let js_mod = match Sys.getenv_opt "ALPINE_MODULE" with
  | Some s -> Ppx_deriving_jsoo_lib.Ppx_js.wrapper := Some s; ref s
  | _ -> ref "Ezjs_min"

let jstyp ~loc s arg =
  ptyp_constr ~loc {txt=Longident.parse (!js_mod ^ "." ^ s); loc} arg
let jsid ~loc s = evar ~loc (!js_mod ^ "." ^ s)
let jsapp ~loc s l = eapply ~loc (jsid ~loc s) l

let get_str_pat p = match p.ppat_desc with
  | Ppat_var {txt; _} -> txt
  | Ppat_any -> "_"
  | Ppat_constraint ({ppat_desc=Ppat_var {txt; _}; _}, _) -> txt
  | Ppat_constraint ({ppat_desc=Ppat_any; _}, _) -> "_"
  | Ppat_construct ({txt=Lident "()"; _}, _) -> "_instance"
  | _ ->
    Location.raise_errorf ~loc:p.ppat_loc "pattern expected to be '_', a variable or a constraint of these patterns"

let remove_poly c = match c.ptyp_desc with
  | Ptyp_poly (_, c) -> c
  | _ -> c

let rec get_exit_type e = match e.pexp_desc with
  | Pexp_fun (_, _, _, e) -> get_exit_type e
  | Pexp_constraint (_, c) -> Some (remove_poly c)
  | _ -> None

let infer_type ~kind e =
  let loc = e.pexp_loc in
  match e.pexp_desc with
  | Pexp_construct ({txt=Lident "()"; _}, None) -> Some [%type: unit]
  | Pexp_constant Pconst_string _ -> Some [%type: string]
  | Pexp_constant Pconst_integer (_, None) -> Some [%type: int]
  | Pexp_constant Pconst_integer (_, Some 'l') -> Some [%type: int32]
  | Pexp_constant Pconst_integer (_, Some 'L') -> Some [%type: int64]
  | Pexp_constant Pconst_integer (_, Some 'n') -> Some [%type: nativeint]
  | Pexp_constant Pconst_float _ -> Some [%type: float]
  | Pexp_constant Pconst_char _ -> Some [%type: char]
  | Pexp_construct ({txt=Lident ("true"|"false"); _}, None) -> Some [%type: bool]
  | Pexp_fun _ when kind = `meth || kind = `func -> Some [%type: unit]
  | _ -> None

let rec add_data ?(kind=`data) acc = function
  | vb :: tl ->
    let name = get_str_pat vb.pvb_pat in
    let conv = acc.convert && not (List.exists (fun a -> a.attr_name.txt = "noconv" || a.attr_name.txt = "noconvert") vb.pvb_attributes) in
    let typ = Option.fold ~none:(infer_type ~kind vb.pvb_expr) ~some:Option.some @@ get_exit_type vb.pvb_expr in
    add_data { acc with data = {name; typ; conv; value=vb.pvb_expr; kind} :: acc.data } tl
  | [] -> { acc with data = List.rev acc.data }

let rec add_store acc = function
  | vb :: tl ->
    let name = get_str_pat vb.pvb_pat in
    let conv = acc.convert && not (List.exists (fun a -> a.attr_name.txt = "noconv" || a.attr_name.txt = "noconvert") vb.pvb_attributes) in
    let kind = match vb.pvb_expr.pexp_desc with Pexp_fun _ -> `func | _ -> `data in
    let typ = Option.fold ~none:(infer_type ~kind vb.pvb_expr) ~some:Option.some @@ get_exit_type vb.pvb_expr in
    add_store { acc with store = {name; typ; conv; value=vb.pvb_expr; kind} :: acc.store } tl
  | [] -> { acc with store = List.rev acc.store }

let get_list_expression e =
  let rec aux acc e = match e.pexp_desc with
    | Pexp_construct ({txt=Lident "[]"; _}, None) -> Some (List.rev acc)
    | Pexp_construct ({txt=Lident "::"; _}, Some {pexp_desc=Pexp_tuple [e1; e2]; _}) ->
      aux (e1 :: acc) e2
    | _ -> None in
  aux [] e

let parse_init_modules acc e = match get_list_expression e with
  | None -> acc.modules_init
  | Some l ->
    List.filter_map (function {pexp_desc=Pexp_construct ({txt; _}, None); _} -> Some txt | _ -> None) l

let parse_initialized_modules acc e = match get_list_expression e with
  | None -> acc.modules_initialized
  | Some l ->
    List.filter_map (function {pexp_desc=Pexp_construct ({txt; _}, None); _} -> Some txt | _ -> None) l

let parse_options acc p = match p with
  | PStr [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_record (l, _); _}, _); _} ] ->
    List.fold_left (fun acc ({txt; _}, e) -> match Longident.name txt, e.pexp_desc with
      | "parent", Pexp_construct ({txt; _}, None) -> { acc with parent = Some txt }
      | ("glob"|"global"|"store"), Pexp_construct ({txt; _}, None) -> { acc with glob = Some txt }
      | ("glob"|"global"|"store"), _ -> { acc with glob = Some (Lident "") }
      | "init", _ -> { acc with root_init = Some (parse_init_modules acc e) }
      | "initialized", _ -> { acc with root_initialized = Some (parse_initialized_modules acc e) }
      | ("debug"|"dbg"), _ -> { acc with debug = true }
      | "noconv", _ -> { acc with convert = false }
      | s, _ -> Format.eprintf "warning: unknown option %S@." s; acc) acc l
  | _ -> acc

let fold =
  object(self)
    inherit [acc] Ast_traverse.fold as super
    method! structure_item it acc =
      match it.pstr_desc with
      | Pstr_extension (({txt=("data"|"alpine.data"); _}, PStr [ {pstr_desc=Pstr_value (_, l); _} ]), _) ->
        let acc = add_data acc l in
        List.fold_left (fun acc vb -> self#value_binding vb acc) acc l
      | Pstr_extension (({txt=("store"|"alpine.store"); _}, PStr [ {pstr_desc=Pstr_value (_, l); _} ]), _) ->
        let acc = add_store acc l in
        List.fold_left (fun acc vb -> self#value_binding vb acc) acc l
      | Pstr_extension (({txt=("meth"|"alpine.meth"); _}, PStr [ {pstr_desc=Pstr_value (_, l); _} ]), _) ->
        let acc = add_data ~kind:`meth acc l in
        List.fold_left (fun acc vb -> self#value_binding vb acc) acc l
      | Pstr_module {pmb_name={txt=Some modu; _}; pmb_expr={pmod_desc=Pmod_structure st; _}; _} ->
        let acc_mod = self#structure st { empty_acc with modu } in
        let modules_init = match acc_mod.init, acc_mod.data, acc_mod.store with
          | None, [], [] -> acc.modules_init @ acc_mod.modules_init
          | _ -> acc.modules_init @ (Lident modu :: acc_mod.modules_init) in
        let modules_initialized = match acc_mod.initialized with
          | None -> acc.modules_initialized @ acc_mod.modules_initialized
          | _ -> acc.modules_initialized @ (Lident modu :: acc_mod.modules_initialized) in
        { acc with modules_init; modules_initialized }
      | Pstr_extension (({txt=("alpine"|"alp"); _}, p), _) -> parse_options acc p
      | Pstr_extension (({txt=("init"|"alpine.init"); _}, PStr [ {pstr_desc=Pstr_eval (f, _); _} ]), _) ->
        { acc with init = Some f }
      | Pstr_extension (({txt=("initialized"|"alpine.initialized"); _}, PStr [ {pstr_desc=Pstr_eval (f, _); _} ]), _) ->
        { acc with initialized = Some f }
      | Pstr_extension (({txt=("data"|"alpine.data"); _}, PStr [ {pstr_desc=Pstr_type (_, l); _} ]), _) ->
        let extra = List.filter_map (function
          | {ptype_manifest = Some c; ptype_name={txt; loc}; ptype_attributes; _} ->
            let conv = acc.convert && not (List.exists (fun a -> a.attr_name.txt = "noconv" || a.attr_name.txt = "noconvert") ptype_attributes) in
            Some { name=txt; typ=Some c; value=eunit ~loc; conv; kind=`data }
          | _ -> None) l in
        { acc with extra_data = acc.extra_data @ extra }
      | _ -> super#structure_item it acc
  end

let converted_type el =
  let open Ppx_deriving_jsoo_lib in
  let open Common in
  match el with
  | { conv=true; typ=Some {ptyp_desc=Ptyp_constr ({txt=Lident ("unit"|"int"); _}, []); _}; _} ->
    el.typ
  | { conv=true; typ=Some c; _ } ->
    let options = { default_options with g_rm_prefix=0 } in
    (match Jsoo_type.type_of_core ~options c with
     | TT c, _ -> Some c
     | CT c, _ -> Some (jstyp ~loc:c.ptyp_loc "t" [ c ]))
  | { typ; _ } -> typ

let function_type e =
  let loc = e.pexp_loc in
  let rec aux e = match e.pexp_desc with
    | Pexp_fun (_, _, {ppat_desc=Ppat_constraint (_, c); _}, e) ->
      ptyp_arrow ~loc Nolabel (remove_poly c) (aux e)
    | Pexp_fun (_, _, {ppat_desc=Ppat_construct ({txt=Lident "()"; _}, _); _}, e) ->
      ptyp_arrow ~loc Nolabel [%type: unit] (aux e)
    | Pexp_fun (_, _, _, e) ->
      ptyp_arrow ~loc Nolabel (ptyp_any ~loc) (aux e)
    | Pexp_constraint (_, c) -> remove_poly c
    | _ -> ptyp_any ~loc in
  aux e

let converted_expr ?(of_=false) el =
  let open Ppx_deriving_jsoo_lib in
  let open Common in
  match el with
  | { conv=true; value; kind=`func; _ } ->
    let c = function_type value in
    let options = { default_options with g_rm_prefix=0 } in
    let m = Jsoo_conv.expr_of_core ~options c in
    let conv = if of_ then m.e_of else m.e_to in
    eapply ~loc:value.pexp_loc conv [ value ]
  | { conv=true; typ=Some c; value; _ } ->
    let c = remove_poly c in
    let options = { default_options with g_rm_prefix=0 } in
    let m = Jsoo_conv.expr_of_core ~options c in
    let conv = if of_ then m.e_of else m.e_to in
    eapply ~loc:value.pexp_loc conv [ value ]
  | { value; _ } -> value

let rec wrap_method ?this ?(pats=[]) el =
  let loc = el.value.pexp_loc in
  match el.value.pexp_desc, this with
  | Pexp_fun (_, _, p, value), None ->
    wrap_method ~this:(get_str_pat p) { el with value }
  | Pexp_fun (_, _, p, value), Some this ->
    let p2 = match el.conv, p.ppat_desc with
      | true, Ppat_constraint (p, c) ->
        ppat_constraint ~loc:p.ppat_loc p (Option.get @@ converted_type { el with typ=Some c })
      | _ -> p in
    pexp_fun ~loc Nolabel None p2 (wrap_method ~this ~pats:(pats @ [p]) { el with value })
  | _, Some _ ->
    let loc = el.value.pexp_loc in
    let this = jsapp ~loc "Unsafe.coerce" [ evar ~loc "_this" ] in
    let args = List.map (fun p ->
      let loc = p.ppat_loc in
      let value, typ = match p.ppat_desc with
        | Ppat_var {txt; _} -> evar ~loc txt, None
        | Ppat_any -> evar ~loc "_", None
        | Ppat_constraint ({ppat_desc=Ppat_var {txt; _}; _}, c) -> evar ~loc txt, Some c
        | Ppat_constraint ({ppat_desc=Ppat_any; _}, c) -> evar ~loc "_", Some c
        | _ -> Location.raise_errorf ~loc "pattern expected to be '_', a variable or a constraint of these patterns" in
      converted_expr ~of_:true {el with value; typ}
    ) pats in
    let value = eapply ~loc (evar ~loc el.name) (this :: args) in
    converted_expr { el with value }
  | _ -> Location.raise_errorf ~loc "unexpected expression for method"

let field_name s =
  let s = match String.rindex_opt s '_' with
    | None -> s
    | Some i when i = String.length s - 1 -> s
    | Some _ -> s ^ "_" in
  match String.get s 0 with
  | 'A'..'Z' -> "_" ^ s
  | _ | exception _ -> s

let object_field el =
  let loc = el.value.pexp_loc in
  match el.kind with
  | `func -> Location.raise_errorf ~loc:el.value.pexp_loc "func kind is not allowed in data"
  | `meth -> pcf_method ~loc ({txt=field_name el.name; loc}, Public, Cfk_concrete (Fresh, wrap_method el))
  | `data -> match el.value.pexp_desc with
    | Pexp_fun _ -> pcf_val ~loc ({txt=field_name el.name; loc}, Immutable, Cfk_concrete (Fresh, wrap_method el))
    | _ -> pcf_val ~loc ({txt=field_name el.name; loc}, Mutable, Cfk_concrete (Fresh, converted_expr el))

let object_ ~loc l =
  let fields = List.map object_field l in
  let cs = class_structure ~self:(ppat_any ~loc) ~fields in
  pexp_extension ~loc ({txt="js";loc}, PStr [ pstr_eval ~loc (pexp_object ~loc cs) [] ])

let data_init ~loc acc =
  match acc.data with
  | [] -> None
  | data ->
    Some [%expr Alpine.fdata _alpine [%e estring ~loc acc.modu] @@ fun _this ->
      [%e object_ ~loc data]]

let store_init ~loc acc =
  List.map (fun el ->
    Some [%expr Alpine.astore _alpine [%e estring ~loc el.name] [%e converted_expr el]]) acc.store

let init ~loc acc =
  let loc = { loc with loc_ghost = true } in
  let init = Option.map (fun e -> eapply ~loc e [[%expr _alpine]]) acc.init in
  let l = List.filter_map Fun.id (data_init ~loc acc :: store_init ~loc acc @ [ init ]) in
  match acc.data, acc.extra_data, acc.store, acc.init with
  | [], [], [], None -> []
  | _ -> [%str let init _alpine = [%e esequence ~loc l]]

let initialized ~loc acc =
  let loc = { loc with loc_ghost = true } in
  match acc.initialized with
  | None -> []
  | Some f -> [%str let initialized _alpine = [%e f] _alpine]

let root ~loc acc =
  let loc = { loc with loc_ghost = true } in
  let l_init = match acc.root_init with
    | None -> []
    | Some root ->
      let l_init = List.map (fun lid ->
        eapply ~loc (pexp_ident ~loc {txt=Ldot (lid, "init"); loc}) [evar ~loc "_alpine"]) root in
      match acc.data, acc.store with
      | [], [] -> l_init
      | _ -> l_init @ [ [%expr init _alpine] ] in
  let l_initialized = match acc.root_initialized with
    | None -> []
    | Some root ->
      List.map (fun lid ->
        eapply ~loc (pexp_ident ~loc {txt=Ldot (lid, "initialized"); loc}) [evar ~loc "_alpine"]) root in
  match l_init, l_initialized with
  | [], [] -> []
  | l_init, [] -> [%str let () = Alpine.init (fun _alpine -> [%e esequence ~loc l_init])]
  | [], l_initialized -> [%str let () = Alpine.initialized (fun _alpine -> [%e esequence ~loc l_initialized])]
  | _ ->
    [%str let () =
            Alpine.init (fun _alpine -> [%e esequence ~loc l_init]);
            Alpine.initialized (fun _alpine -> [%e esequence ~loc l_initialized])]

let conclude ~loc acc =
  let init_str = init ~loc acc in
  let initialized_str = initialized ~loc acc in
  let root_str = root ~loc acc in
  root_str @ init_str @ initialized_str

let type_fields ~loc ~kind acc =
  let rec aux_meth ?(first=true) e = match e.pexp_desc, first with
    | Pexp_fun (_, _, _, e), true -> aux_meth ~first:false e
    | Pexp_fun (_, _, {ppat_desc=Ppat_constraint (_, c); _}, e), _ ->
      ptyp_arrow ~loc Nolabel (remove_poly c) (aux_meth ~first:false e)
    | Pexp_fun (_, _, {ppat_desc=Ppat_construct ({txt=Lident "()"; _}, _); _}, e), _ ->
      ptyp_arrow ~loc Nolabel [%type: unit] (aux_meth ~first:false e)
    | Pexp_fun (_, _, _, e), _ ->
      ptyp_arrow ~loc Nolabel (ptyp_any ~loc) (aux_meth ~first:false e)
    | Pexp_constraint (_, c), _ -> jstyp ~loc "meth" [remove_poly c]
    | _ -> jstyp ~loc "meth" [[%type: unit]] in
  List.map (fun el ->
    match el.kind with
    | `meth ->
      let c = aux_meth el.value in
      otag ~loc {txt=field_name el.name; loc} c
    | `data ->
      let c = Option.value ~default:(ptyp_any ~loc) @@ converted_type el in
      let prop_kind = match el.value.pexp_desc with Pexp_fun _ -> "readonly_prop" | _ -> "prop" in
      otag ~loc {txt=field_name el.name; loc} @@
      jstyp ~loc prop_kind [c]
    | `func ->
      let c = function_type el.value in
      otag ~loc {txt=field_name el.name; loc} @@ jstyp ~loc "prop" [c]
  ) (match kind with `data -> acc.data | `store -> acc.store)

let object_type ~loc ~kind acc =
  ptyp_object ~loc (type_fields ~loc ~kind acc) Closed

let alpine_type ~loc ~kind acc =
  let extra = List.map (fun el ->
    let c = converted_type el in
    Option.map (fun c ->
      otag ~loc {txt=field_name el.name; loc} @@
      jstyp ~loc "readonly_prop" [c]
    ) c
  ) acc.extra_data in
  let data_type_fields =
    (match acc.data, kind with [], _ | _, `data -> None | _ -> Some (oinherit ~loc [%type: data])) ::
    (Option.map (fun parent -> oinherit ~loc (ptyp_constr ~loc {txt=Ldot (parent, "data"); loc} [])) acc.parent) ::
    extra in
  let data_type = match acc.data, acc.extra_data, acc.parent, kind with
    | [], [], None, _ | _, [], None, `data -> ptyp_any ~loc
    | _ -> ptyp_object ~loc (List.filter_map Fun.id data_type_fields) Closed in
  let store_type = match acc.store, acc.glob with
    | [], None -> ptyp_any ~loc
    | [], Some lid when lid <> (Lident "") -> ptyp_constr ~loc {txt=Ldot (lid, "store"); loc} []
    | _ -> [%type: store] in
  jstyp ~loc "t" [
    [%type:
      ([%t jstyp ~loc "t" [data_type]], [%t jstyp ~loc "t" [store_type]]) Alpine.instance ] ]

let wrap_this ~kind acc e =
  let loc = e.pexp_loc in
  let types = alpine_type ~loc ~kind acc in
  match e.pexp_desc with
  | Pexp_fun (_, _, {ppat_desc=Ppat_construct ({txt=Lident "()"; _}, None); _}, e) ->
    [%expr fun (_instance: [%t types]) -> [%e e]], { acc with instance= Some "_instance" }
  | _ ->
    [%expr fun (_instance: [%t types]) -> [%e e]], { acc with instance= Some "_instance" }

let rec debug_expr e = match e.pexp_desc with
  | Pexp_fun (a, l, p, e2) ->
    { e with pexp_desc = Pexp_fun (a, l, p, debug_expr e2) }
  | _ -> evar ~loc:e.pexp_loc "..."

let map =
  object(self)
    inherit [acc] Ast_traverse.fold_map as super
    method! expression e acc =
      match e.pexp_desc, acc.instance with
      | Pexp_extension (
        {txt=("dispatch"|"alpine.dispatch"); loc},
        PStr [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_apply (event, [_, msg]); _}, _); _} ]), Some instance ->
        [%expr Alpine.dispatch ~msg:[%e msg] [%e evar ~loc instance] [%e event]], acc
      | Pexp_extension (
        {txt=("dispatch"|"alpine.dispatch"); loc},
        PStr [ {pstr_desc=Pstr_eval (event, _); _} ]), Some instance ->
        [%expr Alpine.dispatch [%e evar ~loc instance] [%e event]], acc
      | Pexp_extension ({txt=("el"|"alpine.el"); loc}, PStr []), Some instance ->
        [%expr Alpine.el [%e evar ~loc instance]], acc
      | Pexp_extension ({txt=("data"|"alpine.data"); loc}, PStr []), Some instance ->
        [%expr Alpine.data [%e evar ~loc instance]], acc
      | Pexp_extension ({txt=("store"|"alpine.store"); loc}, PStr []), Some instance ->
        [%expr Alpine.store [%e evar ~loc instance]], acc
      | Pexp_extension ({txt=("root"|"alpine.root"); loc}, PStr []), Some instance ->
        [%expr Alpine.root [%e evar ~loc instance]], acc
      | Pexp_extension (
        {txt=("refs"|"alpine.refs"); loc},
        PStr [ {pstr_desc=Pstr_eval (member, _); _} ]), Some instance ->
        [%expr Alpine.refs [%e evar ~loc instance] [%e member]], acc
      | Pexp_extension (
        {txt=("id"|"alpine.id"); loc},
        PStr [ {pstr_desc=Pstr_eval (id, _); _} ]), Some instance ->
        [%expr Alpine.dispatch [%e evar ~loc instance] [%e id]], acc
      | Pexp_extension (
        {txt=("next"|"alpine.next"); loc},
        PStr [ {pstr_desc=Pstr_eval (f, _); _} ]), Some instance ->
        [%expr Alpine.next_tick [%e evar ~loc instance] (fun () -> [%e f])], acc
      | Pexp_extension (
        {txt=("persist"|"alpine.persist"); loc},
        PStr [ {pstr_desc=Pstr_eval (e, _); _} ]), Some instance ->
        [%expr Alpine.persist [%e evar ~loc instance] [%e e]], acc
      | Pexp_extension (
        {txt=("watch"|"alpine.watch"); loc},
        PStr [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_apply (target, [_, f]); _}, _); _} ]), Some instance ->
        [%expr Alpine.watch [%e evar ~loc instance] [%e target] [%e f]], acc
      | _ -> super#expression e acc

    method! structure s acc =
      match s with
      | [] -> [], acc
      | { pstr_loc=loc; _ } :: _ ->
        let loc = { loc with loc_ghost = true } in
        let acc = fold#structure s acc in
        let data_type = match acc.data with [] -> [] | _ -> [%str type data = [%t object_type ~loc ~kind:`data acc]] in
        let store_type = match acc.store with [] -> [] | _ -> [%str type store = [%t object_type ~loc ~kind:`store acc]] in
        let types = store_type @ data_type in
        let s, init_done, ds, first = List.fold_left (fun (s, init_done, ds, first) it ->
          let nfirst, start_str = if first then false, types else first, [] in
          match it.pstr_desc with
          | Pstr_extension (({txt=("data"|"alpine.data"|"meth"|"alpine.meth") as txt; _}, PStr [ {pstr_desc=Pstr_value (rec_flag, l); pstr_loc; _} ]), _) ->
            let l = List.filter_map (fun vb ->
              match vb.pvb_expr.pexp_desc with
              | Pexp_fun _ ->
                let kind = match txt with "data" | "alpine.data" -> `data | _ -> `meth in
                let pvb_expr, acc = wrap_this ~kind acc vb.pvb_expr in
                let vb = fst @@ self#value_binding {vb with pvb_expr} acc in
                Some vb
              | _ -> None) l in
            begin match l with
              | [] -> start_str @ s, init_done, start_str @ ds, nfirst
              | _ ->
                (pstr_value ~loc:pstr_loc rec_flag l) :: start_str @ s, init_done,
                (if acc.debug then (pstr_value ~loc:pstr_loc rec_flag l) :: start_str @ ds else ds),
                nfirst
            end
          | Pstr_extension (({txt=("store"|"alpine.store"); _}, _), _) ->
            start_str @ s, init_done, start_str @ ds, nfirst
          | Pstr_module {pmb_name={txt=Some modu; loc}; pmb_expr={pmod_desc=Pmod_structure st; _}; _} ->
            let it = pstr_module ~loc @@
              module_binding ~loc ~name:{txt=Some modu; loc}
                ~expr:(pmod_structure ~loc (fst @@ self#structure st {empty_acc with modu=String.uncapitalize_ascii modu})) in
            it :: s, init_done, ds, first
          | Pstr_extension (({txt=("alpine"|"alp"); loc}, _), _) ->
            let concl_str = conclude ~loc acc in
            concl_str @ start_str @ s, true,
            (if acc.debug then concl_str @ start_str @ ds else ds), nfirst
          | Pstr_extension (({txt=("init"|"alpine.init"|"initialized"|"alpine.initialized"); _}, _), _)
          | Pstr_extension (({txt=("data"|"alpine.data"); _}, PStr [ {pstr_desc=Pstr_type _; _} ]), _) ->
            s, init_done, ds, first
          | _ -> it :: s, init_done, ds, first)
          ([], false, [], true) s in
        let s, ds =
          if init_done then List.rev s, List.rev ds
          else
            let start_str = if first then types else [] in
            let concl_str = conclude ~loc acc in
            let s = match s with
              | ({ pstr_desc = Pstr_value (_, [ {pvb_pat = {ppat_desc = Ppat_construct ({txt=Lident "()"; _}, _); _}; _}]); _ } as end_) :: tl ->
                end_ :: concl_str @ start_str @ tl
              | _ -> concl_str @ start_str @ s in
            let ds = concl_str @ start_str @ ds in
            List.rev s, List.rev ds in
        if acc.debug then Format.eprintf "%a@." Pprintast.structure ds;
        s, acc
  end

let () =
  Driver.register_transformation "alpine"
    ~impl:(fun s ->
        let modu = match s with
          | [] -> "default"
          | h :: _ ->
            let path = h.pstr_loc.loc_start.pos_fname in
            String.uncapitalize_ascii Filename.(remove_extension @@ basename path) in
        let s = fst @@ map#structure s {empty_acc with modu} in
        s
      )
